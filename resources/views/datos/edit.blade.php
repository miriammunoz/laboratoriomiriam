@extends('layouts.app')

@section('content')

<form action="{{route('datos.update',$dato->id)}}" method="POST">
{{method_field('PATCH')}}
@csrf
<div class="container">
  <div class="form-group">
    <input type="text" class="form-control" name="nombre" value="{{$dato->nombre}}" placeholder="Nombre">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="apellidopaterno" value="{{$dato->apellidopaterno}}" placeholder="Apellido Paterno">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="apellidomaterno" value="{{$dato->apellidomaterno}}" placeholder="Apellido Materno">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="fechanacimiento" value="{{$dato->fechanacimiento}}" placeholder="Fecha de Nacimiento">
  </div>
  <center><button type="submit" class="btn btn-primary col-md-4">Guardar</button></center>
</div>
</form>

@endsection