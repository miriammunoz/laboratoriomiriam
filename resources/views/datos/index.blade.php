@extends('layouts.app')

@section('content')

<div class="text-center">
  <h1>Pacientes</h1>
  <br>
</div>

<div class="container">
  <table class="table table-bordered">
    <thead class="thead-dark">
     <tr>
       <th>ID</th>
       <th>NOMBRE</th>
       <th>APELLIDO PATERNO</th>
       <th>APELLIDO MATERNO</th>
       <th>FECHA NACIMIENTO</th>
       <th>ACCIÓN</th>
     </tr>
    </thead>
    <tbody>
    @foreach($datos as $dato)
    </tr>
      <td>{{$dato->id}}</td>
      <td>{{$dato->nombre}}</td>
      <td>{{$dato->apellidopaterno}}</td>
      <td>{{$dato->apellidomaterno}}</td>
      <td>{{$dato->fechanacimiento}}</td>
      <td>
        <a href="{{url('/datos/'.$dato->id.'/edit')}}" class="btn btn-link">Editar</a>
      </td>
    </tr>
    @endforeach
    </tbody>
  </table>
  <center><a class="btn btn-primary col-md-4" href="{{url('/datos/create')}}">Agregar</a></center>
</div>


@endsection