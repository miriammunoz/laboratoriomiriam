@extends('layouts.app')

@section('content')

<form action="{{route('datos.store')}}" method="POST">
@csrf
<div class="container">
  <div class="form-group">
    <input type="text" class="form-control" name="nombre" placeholder="Nombre">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="apellidopaterno" placeholder="Apellido Paterno">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="apellidomaterno" placeholder="Apellido Materno">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="fechanacimiento" placeholder="Fecha de Nacimiento">
  </div>
  <center><button type="submit" class="btn btn-primary col-md-4">Guardar</button></center>
</div>
</form>

@endsection